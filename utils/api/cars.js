/* eslint-disable no-return-await */
import { getRequest } from './requests';

export default {
  async getAll(params) {
    return await getRequest('car-listings', { params });
  },
  async get(id) {
    return await getRequest('car-listings', { params: { listingId: id } });
  },
  async getMakers() {
    return await getRequest('car-makers');
  },
  async getModels() {
    return await getRequest('car-listing-models');
  },
  async getTrims() {
    return await getRequest('car-trims');
  },
  async sendCheckAvailability(params) {
    return await getRequest('ListingInquiry', params);
  },
};
