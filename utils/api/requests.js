import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.cargradient.com/',
});

export async function getRequest(path, params = {}) {
  try {
    return (await api.get(path, params)).data;
  } catch (e) {
    return e;
  }
}

export async function postRequest(path, data, params = {}) {
  try {
    return (await api.post(path, data, params)).data;
  } catch (e) {
    return e;
  }
}

export async function putRequest(path, data, params = {}) {
  try {
    return (await api.put(path, data, params)).data;
  } catch (e) {
    return e;
  }
}

export async function deleteRequest(path, params = {}) {
  try {
    return (await api.delete(path, params)).data;
  } catch (e) {
    return e;
  }
}
