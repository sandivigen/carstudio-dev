import * as types from './mutation-types';
import createBlockingAction from '~/helpers/vuex/createBlockingAction';

const ITEMS_CLEANUP_TTL = 1000 * 60 * 10; // 10 min

export default {
  addItem({ commit }, data) {
    commit(types.ADD_ITEM, data);
  },
  updateItem({ commit }, item) {
    commit(types.UPDATE_ITEM, item);
  },
  removeItem({ commit }, item) {
    commit(types.REMOVE_ITEM, item);
  },
  clearItems({ commit }) {
    commit(types.CLEAR_ITEMS);
  },
  updateItems({ commit }, items) {
    commit(types.UPDATE_ITEMS, items);
  },
  cleanUpItems: createBlockingAction({
    async action({ state, commit }) {
      const now = Date.now();
      if (now < state.itemsCleanedUpAt + ITEMS_CLEANUP_TTL) return;

      commit(types.SET_CLEANING_UP_ITEMS, true);

      const results = await Promise.all(
        state.items
          .filter(({ specs }) => !!specs)
          .map((item) => this.$apiClient.getCarSpecs({
            query: {
              car_make: item.make,
              car_model: item.model,
              car_year: item.year,
              car_trim: item.specs.BasicSpec_Trim,
            },
          })),
      );

      commit(types.UPDATE_ITEMS, results.map(({ data }) => {
        const [specs] = data;

        if (!specs) return null;

        return {
          make: specs.BasicSpec_Make,
          model: specs.BasicSpec_Model,
          year: specs.BasicSpec_Year,
          specs,
        };
      }).filter(Boolean));
      commit(types.SET_CLEANING_UP_ITEMS, false);
      commit(types.SET_ITEMS_CLEANED_UP_AT, now);
    },
  }),
};
