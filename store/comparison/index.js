import mutations from './mutations';
import actions from './actions';
import getters from './getters';
import state from './state';

export default {
  state,
  actions,
  getters,
  mutations,
  namespaced: true,
};
