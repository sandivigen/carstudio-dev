export default {
  completedItems(state) {
    return state.items.filter(({ specs }) => !!specs);
  },
};
