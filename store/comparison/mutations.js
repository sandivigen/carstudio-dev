import _merge from 'lodash/merge';
import _keys from 'lodash/keys';
import _pick from 'lodash/pick';
import _cloneDeep from 'lodash/cloneDeep';
import { nanoid } from 'nanoid';

import * as types from './mutation-types';

const ITEM_MODEL = {
  $id: '',
  make: '',
  model: '',
  year: '',
  specs: null,
};

function createItem(data = {}) {
  const item = _merge(
    _cloneDeep(ITEM_MODEL),
    _pick(data, _keys(ITEM_MODEL)),
  );

  if (!item.$id) item.$id = nanoid();

  return item;
}

// Use create item in each mutation to make sure data is correct

export default {
  [types.ADD_ITEM](state, data = {}) {
    state.items.push(createItem(data));
  },

  [types.UPDATE_ITEM](state, update) {
    const index = state.items.findIndex(({ $id }) => $id === update.$id);
    if (index > -1) {
      state.items.splice(index, 1, createItem(update));
    }
  },

  [types.REMOVE_ITEM](state, item) {
    const index = state.items.findIndex(({ $id }) => $id === item.$id);
    if (index > -1) {
      state.items.splice(index, 1);
    }
  },

  [types.CLEAR_ITEMS](state) {
    state.items = [createItem()];
  },

  [types.UPDATE_ITEMS](state, items = []) {
    state.items = items.length ? items.map(createItem) : [createItem()];
  },

  [types.SET_CLEANING_UP_ITEMS](state, value) {
    state.cleaningUpItems = !!value;
  },

  [types.SET_ITEMS_CLEANED_UP_AT](state, timestamp) {
    state.itemsCleanedUpAt = timestamp;
  },
};
