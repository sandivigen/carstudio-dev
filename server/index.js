const express = require('express');
const { Nuxt, Builder } = require('nuxt');

const config = require('../nuxt.config');

const app = express();

const isProduction = process.env.NODE_ENV === 'production';

async function start() {
  const nuxt = new Nuxt(config);

  const { host, port } = nuxt.options.server;

  if (isProduction) {
    await nuxt.ready();
  } else {
    const builder = new Builder(nuxt);
    await builder.build();
  }

  app.use(nuxt.render);
  app.listen(port, host);
  console.log(`Server listening on http://${host}:${port}`); // eslint-disable-line no-console
}
start();
