module.exports = {
  root: true,
  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
  ],
  rules: {
    'class-methods-use-this': 'off',
    'import/no-named-as-default': 'off',
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'no-param-reassign': ['error', { props: false }],
    'no-plusplus': 'off',
    'no-underscore-dangle': 'off',
    'prefer-destructuring': 'off',
    'vue/max-len': ['error', {
      code: 100,
      tabWidth: 2,
      ignoreComments: true,
      ignoreTrailingComments: true,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
      template: 500,
      ignoreHTMLAttributeValues: true,
      ignoreHTMLTextContents: false,
    }],
    'vue/no-v-html': 'off',
    'vue/script-indent': ['error', 2, {
      baseIndent: 0,
    }],
    'vue/max-attributes-per-line': 'off',
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
  },
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        indent: 'off',
        'max-len': 'off',
      },
    },
    {
      files: ['*.js'],
      rules: {
        'vue/max-len': 'off',
      },
    },
  ],
  settings: {
    'import/resolver': {
      nuxt: {
        extensions: ['.js', '.vue'],
      },
    },
  },
};
