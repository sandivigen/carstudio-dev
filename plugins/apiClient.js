import createApiClient from '~/services/apiClient';

export default (context, inject) => {
  const apiClient = createApiClient({
    url: context.$config.apiUrl,
  });

  inject('apiClient', apiClient);
};
