import axios from 'axios';
import pupa from 'pupa';

export default class ApiClient {
  constructor(options) {
    this.transport = axios.create({
      baseURL: options.url,
    });
  }

  formatUrl(urlString, args = {}) {
    return pupa(urlString, args);
  }

  transformResponse(response) {
    return response;
  }

  transformError(err) {
    // TODO: format error
    throw err;
  }
}
