export default {
  getCarMakers: { method: 'get', url: 'car-makers/' },
  getCarModels: { method: 'get', url: 'car-models/' },
  getCarYears: { method: 'get', url: 'car-years/' },
  getCarTrims: { method: 'get', url: 'car-trims/' },
  getCarSpecs: { method: 'get', url: 'car-specs/' },
};
