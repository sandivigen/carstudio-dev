import ApiClient from './ApiClient';
import endpoints from './endpoints';

export default (options) => new Proxy(new ApiClient(options), {
  get(target, name) {
    if (endpoints[name]) {
      return (opts = {}) => {
        const {
          data,
          params,
          query,
          headers,
        } = opts;
        // TODO: allow to define headers and transforms per endpoint
        return target.transport({
          method: endpoints[name].method,
          url: target.formatUrl(endpoints[name].url, params),
          params: query,
          data,
          headers,
        })
          .then(target.transformResponse)
          .catch(target.transformError);
      };
    }

    return target[name];
  },
});
