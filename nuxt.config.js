module.exports = {
  ssr: process.env.SSR === 'true',
  telemetry: false,
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'format-detection', content: 'address=no' },
      { name: 'format-detection', content: 'address=no' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'msapplication-config', content: '/favicons/browserconfig.xml' },
      { name: 'theme-color', content: '#ffffff' },
    ],
    link: [
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicons/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicons/favicon-16x16.png',
      },
      { rel: 'manifest', href: '/favicons/site.webmanifest' },
      { rel: 'shortcut icon', href: '/favicons/favicon.ico' },
      { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#e44d26' },
    ],
  },
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            emitWarning: true,
          },
        });
      }
    },
    postcss: {
      plugins: {
        'postcss-discard-duplicates': {},
      },
    },
  },
  buildModules: [
    '@nuxtjs/vuetify',
    [
      '@nuxtjs/stylelint-module',
      {
        emitWarning: true,
      },
    ],
  ],
  modules: ['@nuxtjs/axios', '@nuxtjs/style-resources'],
  plugins: [
    '~/plugins/apiClient.js',
    { src: '~/plugins/vuexLocalStorage.js', ssr: false },
    { src: '~/plugins/nuxt-swiper-plugin.js', ssr: false },
  ],
  publicRuntimeConfig: {
    forumBaseUrl: process.env.FORUM_URL,
    apiUrl: process.env.API_URL,
  },
  css: ['@/assets/styles/main.scss', 'swiper/css/swiper.css'],
  styleResources: {
    scss: ['@/assets/styles/mixins/*.scss', '@/assets/styles/_variables.scss'],
  },
  server: {
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'localhost',
  },
};
