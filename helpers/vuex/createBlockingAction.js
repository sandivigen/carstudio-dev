/**
 * Prevent calling the same action while previous pending
 * Useful for GET requests
 */
export default function createBlockingAction({ action, key }) {
  const promises = {};

  return function callAction(store, params) {
    const k = (typeof key === 'function' && key(store, params)) || '_default';

    if (promises[k]) {
      return promises[k];
    }

    const promise = new Promise((resolve, reject) => {
      const actionPromise = action.call(this, store, params);

      if (!actionPromise || (!(actionPromise instanceof Promise) && (typeof actionPromise.then !== 'function'))) {
        return resolve(actionPromise);
      }

      return actionPromise
        .then((result) => {
          resolve(result);
          delete promises[k];
        })
        .catch((err) => {
          reject(err);
          delete promises[k];
        });
    });

    promises[k] = promise;

    return promise;
  };
}
