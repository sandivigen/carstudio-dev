export default function splitDigits(value, thousands = ',', decimals = '.') {
  if (value === 0) return '0';
  if (!value) return '';

  return value
    .toString()
    .replace('.', decimals)
    .replace(/\B(?=(\d{3})+(?!\d))/g, thousands);
}
